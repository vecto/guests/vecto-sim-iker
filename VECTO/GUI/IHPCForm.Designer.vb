﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class IHPCForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(IHPCForm))
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.tbModel = New System.Windows.Forms.TextBox()
        Me.pnInertia = New System.Windows.Forms.Panel()
        Me.Label51 = New System.Windows.Forms.Label()
        Me.tbInertia = New System.Windows.Forms.TextBox()
        Me.lblinertiaUnit = New System.Windows.Forms.Label()
        Me.pnThermalOverloadRecovery = New System.Windows.Forms.FlowLayoutPanel()
        Me.Label52 = New System.Windows.Forms.Label()
        Me.tbThermalOverload = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.tbDragCurve = New System.Windows.Forms.TextBox()
        Me.btDragCurve = New System.Windows.Forms.Button()
        Me.btDragCurveOpen = New System.Windows.Forms.Button()
        Me.tcVoltageLevels = New System.Windows.Forms.TabControl()
        Me.tpFirstVoltageLevel = New System.Windows.Forms.TabPage()
        Me.lvPowerMap = New System.Windows.Forms.GroupBox()
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel()
        Me.lvPowerMap1 = New System.Windows.Forms.ListView()
        Me.ColumnHeader4 = New System.Windows.Forms.ColumnHeader()
        Me.ColumnHeader5 = New System.Windows.Forms.ColumnHeader()
        Me.FlowLayoutPanel11 = New System.Windows.Forms.FlowLayoutPanel()
        Me.btAddPowerMap1 = New System.Windows.Forms.Button()
        Me.btRemovePowerMap1 = New System.Windows.Forms.Button()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.FlowLayoutPanel3 = New System.Windows.Forms.FlowLayoutPanel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.tbFLCurve1 = New System.Windows.Forms.TextBox()
        Me.btFLCurveFile1 = New System.Windows.Forms.Button()
        Me.btFLCurve1 = New System.Windows.Forms.Button()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.tbOverloadTime1 = New System.Windows.Forms.TextBox()
        Me.tbVoltage1 = New System.Windows.Forms.TextBox()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.tbContinuousTorque1 = New System.Windows.Forms.TextBox()
        Me.tbContinuousTorqueSpeed1 = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.tbOverloadTorqueSpeed1 = New System.Windows.Forms.TextBox()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.tbOverloadTorque1 = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.tpVoltageLevel = New System.Windows.Forms.TabPage()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.lvPowerMap2 = New System.Windows.Forms.ListView()
        Me.ColumnHeader1 = New System.Windows.Forms.ColumnHeader()
        Me.ColumnHeader2 = New System.Windows.Forms.ColumnHeader()
        Me.FlowLayoutPanel4 = New System.Windows.Forms.FlowLayoutPanel()
        Me.btAddPowerMap2 = New System.Windows.Forms.Button()
        Me.btRemovePowerMap2 = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.FlowLayoutPanel10 = New System.Windows.Forms.FlowLayoutPanel()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.tbFLCurve2 = New System.Windows.Forms.TextBox()
        Me.btFLCurveFile2 = New System.Windows.Forms.Button()
        Me.btFLCurve2 = New System.Windows.Forms.Button()
        Me.TableLayoutPanel4 = New System.Windows.Forms.TableLayoutPanel()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.tbOverloadTime2 = New System.Windows.Forms.TextBox()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.tbVoltage2 = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.tbContinuousTorque2 = New System.Windows.Forms.TextBox()
        Me.tbContinuousTorqueSpeed2 = New System.Windows.Forms.TextBox()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.tbOverloadTorqueSpeed2 = New System.Windows.Forms.TextBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.tbOverloadTorque2 = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.ToolStripBtNew = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripBtOpen = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripBtSave = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripBtSaveAs = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripBtSendTo = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.lblTitle = New System.Windows.Forms.Label()
        Me.btCancel = New System.Windows.Forms.Button()
        Me.btSave = New System.Windows.Forms.Button()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.LbStatus = New System.Windows.Forms.ToolStripStatusLabel()
        Me.CmOpenFile = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.OpenWithToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ShowInFolderToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.lblRatedPower = New System.Windows.Forms.Label()
        Me.tbRatedPower = New System.Windows.Forms.TextBox()
        Me.lblRatedPowerUnit = New System.Windows.Forms.Label()
        Me.FlowLayoutPanel1.SuspendLayout
        Me.pnInertia.SuspendLayout
        Me.pnThermalOverloadRecovery.SuspendLayout
        Me.FlowLayoutPanel2.SuspendLayout
        Me.tcVoltageLevels.SuspendLayout
        Me.tpFirstVoltageLevel.SuspendLayout
        Me.lvPowerMap.SuspendLayout
        Me.TableLayoutPanel3.SuspendLayout
        Me.FlowLayoutPanel11.SuspendLayout
        Me.FlowLayoutPanel3.SuspendLayout
        Me.TableLayoutPanel1.SuspendLayout
        Me.tpVoltageLevel.SuspendLayout
        Me.GroupBox1.SuspendLayout
        Me.TableLayoutPanel2.SuspendLayout
        Me.FlowLayoutPanel4.SuspendLayout
        Me.FlowLayoutPanel10.SuspendLayout
        Me.TableLayoutPanel4.SuspendLayout
        Me.ToolStrip1.SuspendLayout
        CType(Me.PictureBox1,System.ComponentModel.ISupportInitialize).BeginInit
        Me.StatusStrip1.SuspendLayout
        Me.CmOpenFile.SuspendLayout
        Me.Panel1.SuspendLayout
        Me.SuspendLayout
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.Label49)
        Me.FlowLayoutPanel1.Controls.Add(Me.tbModel)
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(14, 89)
        Me.FlowLayoutPanel1.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(598, 30)
        Me.FlowLayoutPanel1.TabIndex = 100
        '
        'Label49
        '
        Me.Label49.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label49.AutoSize = true
        Me.Label49.Location = New System.Drawing.Point(4, 7)
        Me.Label49.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(96, 15)
        Me.Label49.TabIndex = 43
        Me.Label49.Text = "Make and Model"
        '
        'tbModel
        '
        Me.tbModel.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.tbModel.Location = New System.Drawing.Point(108, 3)
        Me.tbModel.Margin = New System.Windows.Forms.Padding(4, 3, 0, 3)
        Me.tbModel.Name = "tbModel"
        Me.tbModel.Size = New System.Drawing.Size(486, 23)
        Me.tbModel.TabIndex = 0
        '
        'pnInertia
        '
        Me.pnInertia.Controls.Add(Me.Label51)
        Me.pnInertia.Controls.Add(Me.tbInertia)
        Me.pnInertia.Controls.Add(Me.lblinertiaUnit)
        Me.pnInertia.Location = New System.Drawing.Point(14, 122)
        Me.pnInertia.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.pnInertia.Name = "pnInertia"
        Me.pnInertia.Size = New System.Drawing.Size(248, 30)
        Me.pnInertia.TabIndex = 101
        '
        'Label51
        '
        Me.Label51.AutoSize = true
        Me.Label51.Location = New System.Drawing.Point(4, 7)
        Me.Label51.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label51.Name = "Label51"
        Me.Label51.Size = New System.Drawing.Size(40, 15)
        Me.Label51.TabIndex = 45
        Me.Label51.Text = "Inertia"
        '
        'tbInertia
        '
        Me.tbInertia.Location = New System.Drawing.Point(125, 3)
        Me.tbInertia.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.tbInertia.Name = "tbInertia"
        Me.tbInertia.Size = New System.Drawing.Size(66, 23)
        Me.tbInertia.TabIndex = 1
        '
        'lblinertiaUnit
        '
        Me.lblinertiaUnit.AutoSize = true
        Me.lblinertiaUnit.Location = New System.Drawing.Point(199, 7)
        Me.lblinertiaUnit.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblinertiaUnit.Name = "lblinertiaUnit"
        Me.lblinertiaUnit.Size = New System.Drawing.Size(43, 15)
        Me.lblinertiaUnit.TabIndex = 64
        Me.lblinertiaUnit.Text = "[kgm²]"
        '
        'pnThermalOverloadRecovery
        '
        Me.pnThermalOverloadRecovery.Controls.Add(Me.Label52)
        Me.pnThermalOverloadRecovery.Controls.Add(Me.tbThermalOverload)
        Me.pnThermalOverloadRecovery.Controls.Add(Me.Label10)
        Me.pnThermalOverloadRecovery.Location = New System.Drawing.Point(266, 122)
        Me.pnThermalOverloadRecovery.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.pnThermalOverloadRecovery.Name = "pnThermalOverloadRecovery"
        Me.pnThermalOverloadRecovery.Size = New System.Drawing.Size(309, 30)
        Me.pnThermalOverloadRecovery.TabIndex = 102
        '
        'Label52
        '
        Me.Label52.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label52.AutoSize = true
        Me.Label52.Location = New System.Drawing.Point(4, 7)
        Me.Label52.Margin = New System.Windows.Forms.Padding(4, 0, 0, 0)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(188, 15)
        Me.Label52.TabIndex = 50
        Me.Label52.Text = "Thermal Overload Recovery Factor"
        '
        'tbThermalOverload
        '
        Me.tbThermalOverload.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.tbThermalOverload.Location = New System.Drawing.Point(198, 3)
        Me.tbThermalOverload.Margin = New System.Windows.Forms.Padding(6, 3, 4, 3)
        Me.tbThermalOverload.Name = "tbThermalOverload"
        Me.tbThermalOverload.Size = New System.Drawing.Size(65, 23)
        Me.tbThermalOverload.TabIndex = 2
        '
        'Label10
        '
        Me.Label10.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label10.AutoSize = true
        Me.Label10.Location = New System.Drawing.Point(269, 7)
        Me.Label10.Margin = New System.Windows.Forms.Padding(2, 0, 4, 0)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(20, 15)
        Me.Label10.TabIndex = 67
        Me.Label10.Text = "[-]"
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.Controls.Add(Me.Label1)
        Me.FlowLayoutPanel2.Controls.Add(Me.tbDragCurve)
        Me.FlowLayoutPanel2.Controls.Add(Me.btDragCurve)
        Me.FlowLayoutPanel2.Controls.Add(Me.btDragCurveOpen)
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(14, 203)
        Me.FlowLayoutPanel2.Margin = New System.Windows.Forms.Padding(0)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(547, 30)
        Me.FlowLayoutPanel2.TabIndex = 103
        '
        'Label1
        '
        Me.Label1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label1.AutoSize = true
        Me.Label1.Location = New System.Drawing.Point(4, 7)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 7, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(110, 15)
        Me.Label1.TabIndex = 59
        Me.Label1.Text = "Drag Curve (.vemd)"
        '
        'tbDragCurve
        '
        Me.tbDragCurve.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.tbDragCurve.Location = New System.Drawing.Point(125, 3)
        Me.tbDragCurve.Margin = New System.Windows.Forms.Padding(4, 3, 7, 3)
        Me.tbDragCurve.Name = "tbDragCurve"
        Me.tbDragCurve.Size = New System.Drawing.Size(322, 23)
        Me.tbDragCurve.TabIndex = 3
        '
        'btDragCurve
        '
        Me.btDragCurve.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btDragCurve.Image = CType(resources.GetObject("btDragCurve.Image"),System.Drawing.Image)
        Me.btDragCurve.Location = New System.Drawing.Point(454, 0)
        Me.btDragCurve.Margin = New System.Windows.Forms.Padding(0)
        Me.btDragCurve.Name = "btDragCurve"
        Me.btDragCurve.Size = New System.Drawing.Size(28, 28)
        Me.btDragCurve.TabIndex = 4
        Me.btDragCurve.UseVisualStyleBackColor = true
        '
        'btDragCurveOpen
        '
        Me.btDragCurveOpen.Image = Global.TUGraz.VECTO.My.Resources.Resources.application_export_icon_small
        Me.btDragCurveOpen.Location = New System.Drawing.Point(482, 1)
        Me.btDragCurveOpen.Margin = New System.Windows.Forms.Padding(0, 1, 0, 0)
        Me.btDragCurveOpen.Name = "btDragCurveOpen"
        Me.btDragCurveOpen.Size = New System.Drawing.Size(28, 28)
        Me.btDragCurveOpen.TabIndex = 84
        Me.btDragCurveOpen.TabStop = false
        Me.btDragCurveOpen.UseVisualStyleBackColor = true
        '
        'tcVoltageLevels
        '
        Me.tcVoltageLevels.Controls.Add(Me.tpFirstVoltageLevel)
        Me.tcVoltageLevels.Controls.Add(Me.tpVoltageLevel)
        Me.tcVoltageLevels.Location = New System.Drawing.Point(14, 244)
        Me.tcVoltageLevels.Margin = New System.Windows.Forms.Padding(0)
        Me.tcVoltageLevels.Name = "tcVoltageLevels"
        Me.tcVoltageLevels.SelectedIndex = 0
        Me.tcVoltageLevels.Size = New System.Drawing.Size(603, 391)
        Me.tcVoltageLevels.TabIndex = 104
        '
        'tpFirstVoltageLevel
        '
        Me.tpFirstVoltageLevel.Controls.Add(Me.lvPowerMap)
        Me.tpFirstVoltageLevel.Controls.Add(Me.FlowLayoutPanel3)
        Me.tpFirstVoltageLevel.Controls.Add(Me.TableLayoutPanel1)
        Me.tpFirstVoltageLevel.Location = New System.Drawing.Point(4, 24)
        Me.tpFirstVoltageLevel.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.tpFirstVoltageLevel.Name = "tpFirstVoltageLevel"
        Me.tpFirstVoltageLevel.Padding = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.tpFirstVoltageLevel.Size = New System.Drawing.Size(595, 363)
        Me.tpFirstVoltageLevel.TabIndex = 0
        Me.tpFirstVoltageLevel.Text = "Voltage Level Low"
        Me.tpFirstVoltageLevel.UseVisualStyleBackColor = true
        '
        'lvPowerMap
        '
        Me.lvPowerMap.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.lvPowerMap.Controls.Add(Me.TableLayoutPanel3)
        Me.lvPowerMap.Location = New System.Drawing.Point(7, 145)
        Me.lvPowerMap.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.lvPowerMap.Name = "lvPowerMap"
        Me.lvPowerMap.Padding = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.lvPowerMap.Size = New System.Drawing.Size(583, 208)
        Me.lvPowerMap.TabIndex = 107
        Me.lvPowerMap.TabStop = false
        Me.lvPowerMap.Text = "Power Map Per Gear"
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.TableLayoutPanel3.ColumnCount = 2
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 419!))
        Me.TableLayoutPanel3.Controls.Add(Me.lvPowerMap1, 0, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.FlowLayoutPanel11, 0, 1)
        Me.TableLayoutPanel3.Controls.Add(Me.Label7, 1, 1)
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(7, 16)
        Me.TableLayoutPanel3.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.RowCount = 2
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 78.61636!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 21.38365!))
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(569, 183)
        Me.TableLayoutPanel3.TabIndex = 108
        '
        'lvPowerMap1
        '
        Me.lvPowerMap1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom)  _
            Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.lvPowerMap1.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader4, Me.ColumnHeader5})
        Me.TableLayoutPanel3.SetColumnSpan(Me.lvPowerMap1, 2)
        Me.lvPowerMap1.FullRowSelect = true
        Me.lvPowerMap1.GridLines = true
        Me.lvPowerMap1.Location = New System.Drawing.Point(4, 3)
        Me.lvPowerMap1.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.lvPowerMap1.MultiSelect = false
        Me.lvPowerMap1.Name = "lvPowerMap1"
        Me.lvPowerMap1.Size = New System.Drawing.Size(561, 137)
        Me.lvPowerMap1.TabIndex = 77
        Me.lvPowerMap1.TabStop = false
        Me.lvPowerMap1.UseCompatibleStateImageBehavior = false
        Me.lvPowerMap1.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Text = "Gear #"
        Me.ColumnHeader4.Width = 59
        '
        'ColumnHeader5
        '
        Me.ColumnHeader5.Text = "Power Map File Name"
        Me.ColumnHeader5.Width = 390
        '
        'FlowLayoutPanel11
        '
        Me.FlowLayoutPanel11.Controls.Add(Me.btAddPowerMap1)
        Me.FlowLayoutPanel11.Controls.Add(Me.btRemovePowerMap1)
        Me.FlowLayoutPanel11.Location = New System.Drawing.Point(4, 146)
        Me.FlowLayoutPanel11.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.FlowLayoutPanel11.Name = "FlowLayoutPanel11"
        Me.FlowLayoutPanel11.Size = New System.Drawing.Size(71, 32)
        Me.FlowLayoutPanel11.TabIndex = 109
        '
        'btAddPowerMap1
        '
        Me.btAddPowerMap1.Image = Global.TUGraz.VECTO.My.Resources.Resources.plus_circle_icon
        Me.btAddPowerMap1.Location = New System.Drawing.Point(4, 3)
        Me.btAddPowerMap1.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.btAddPowerMap1.Name = "btAddPowerMap1"
        Me.btAddPowerMap1.Size = New System.Drawing.Size(28, 28)
        Me.btAddPowerMap1.TabIndex = 38
        Me.btAddPowerMap1.UseVisualStyleBackColor = true
        '
        'btRemovePowerMap1
        '
        Me.btRemovePowerMap1.Image = Global.TUGraz.VECTO.My.Resources.Resources.minus_circle_icon
        Me.btRemovePowerMap1.Location = New System.Drawing.Point(4, 37)
        Me.btRemovePowerMap1.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.btRemovePowerMap1.Name = "btRemovePowerMap1"
        Me.btRemovePowerMap1.Size = New System.Drawing.Size(28, 28)
        Me.btRemovePowerMap1.TabIndex = 39
        Me.btRemovePowerMap1.UseVisualStyleBackColor = true
        '
        'Label7
        '
        Me.Label7.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.Label7.AutoSize = true
        Me.Label7.Location = New System.Drawing.Point(444, 143)
        Me.Label7.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(121, 15)
        Me.Label7.TabIndex = 6
        Me.Label7.Text = "(Double-Click to Edit)"
        '
        'FlowLayoutPanel3
        '
        Me.FlowLayoutPanel3.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.FlowLayoutPanel3.Controls.Add(Me.Label2)
        Me.FlowLayoutPanel3.Controls.Add(Me.tbFLCurve1)
        Me.FlowLayoutPanel3.Controls.Add(Me.btFLCurveFile1)
        Me.FlowLayoutPanel3.Controls.Add(Me.btFLCurve1)
        Me.FlowLayoutPanel3.Location = New System.Drawing.Point(7, 108)
        Me.FlowLayoutPanel3.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.FlowLayoutPanel3.Name = "FlowLayoutPanel3"
        Me.FlowLayoutPanel3.Size = New System.Drawing.Size(579, 30)
        Me.FlowLayoutPanel3.TabIndex = 106
        '
        'Label2
        '
        Me.Label2.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label2.AutoSize = true
        Me.Label2.Location = New System.Drawing.Point(7, 7)
        Me.Label2.Margin = New System.Windows.Forms.Padding(7, 0, 10, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(138, 15)
        Me.Label2.TabIndex = 59
        Me.Label2.Text = "Full Load Curve (.viepcp)"
        '
        'tbFLCurve1
        '
        Me.tbFLCurve1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.tbFLCurve1.Location = New System.Drawing.Point(159, 3)
        Me.tbFLCurve1.Margin = New System.Windows.Forms.Padding(4, 3, 7, 3)
        Me.tbFLCurve1.Name = "tbFLCurve1"
        Me.tbFLCurve1.Size = New System.Drawing.Size(322, 23)
        Me.tbFLCurve1.TabIndex = 36
        '
        'btFLCurveFile1
        '
        Me.btFLCurveFile1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btFLCurveFile1.Image = CType(resources.GetObject("btFLCurveFile1.Image"),System.Drawing.Image)
        Me.btFLCurveFile1.Location = New System.Drawing.Point(488, 0)
        Me.btFLCurveFile1.Margin = New System.Windows.Forms.Padding(0)
        Me.btFLCurveFile1.Name = "btFLCurveFile1"
        Me.btFLCurveFile1.Size = New System.Drawing.Size(28, 28)
        Me.btFLCurveFile1.TabIndex = 37
        Me.btFLCurveFile1.UseVisualStyleBackColor = true
        '
        'btFLCurve1
        '
        Me.btFLCurve1.Image = Global.TUGraz.VECTO.My.Resources.Resources.application_export_icon_small
        Me.btFLCurve1.Location = New System.Drawing.Point(516, 1)
        Me.btFLCurve1.Margin = New System.Windows.Forms.Padding(0, 1, 0, 0)
        Me.btFLCurve1.Name = "btFLCurve1"
        Me.btFLCurve1.Size = New System.Drawing.Size(28, 28)
        Me.btFLCurve1.TabIndex = 85
        Me.btFLCurve1.TabStop = false
        Me.btFLCurve1.UseVisualStyleBackColor = true
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 7
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 163!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 72!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 37!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 147!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 72!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 37!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 67!))
        Me.TableLayoutPanel1.Controls.Add(Me.Label11, 2, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Label4, 2, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Label13, 5, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Label43, 3, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Label37, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.tbOverloadTime1, 4, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.tbVoltage1, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Label39, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Label3, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.tbContinuousTorque1, 1, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.tbContinuousTorqueSpeed1, 1, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.Label15, 2, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.Label42, 3, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.Label14, 5, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.tbOverloadTorqueSpeed1, 4, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.Label41, 3, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.tbOverloadTorque1, 4, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Label12, 5, 1)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(7, 8)
        Me.TableLayoutPanel1.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 3
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(579, 95)
        Me.TableLayoutPanel1.TabIndex = 105
        '
        'Label11
        '
        Me.Label11.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label11.AutoSize = true
        Me.Label11.Location = New System.Drawing.Point(236, 39)
        Me.Label11.Margin = New System.Windows.Forms.Padding(0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(35, 15)
        Me.Label11.TabIndex = 63
        Me.Label11.Text = "[Nm]"
        '
        'Label4
        '
        Me.Label4.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label4.AutoSize = true
        Me.Label4.Location = New System.Drawing.Point(242, 8)
        Me.Label4.Margin = New System.Windows.Forms.Padding(0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(22, 15)
        Me.Label4.TabIndex = 62
        Me.Label4.Text = "[V]"
        '
        'Label13
        '
        Me.Label13.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label13.AutoSize = true
        Me.Label13.Location = New System.Drawing.Point(499, 8)
        Me.Label13.Margin = New System.Windows.Forms.Padding(0)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(20, 15)
        Me.Label13.TabIndex = 65
        Me.Label13.Text = "[s]"
        '
        'Label43
        '
        Me.Label43.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label43.AutoSize = true
        Me.Label43.Location = New System.Drawing.Point(331, 8)
        Me.Label43.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(84, 15)
        Me.Label43.TabIndex = 11
        Me.Label43.Text = "Overload Time"
        '
        'Label37
        '
        Me.Label37.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label37.AutoSize = true
        Me.Label37.Location = New System.Drawing.Point(113, 8)
        Me.Label37.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(46, 15)
        Me.Label37.TabIndex = 1
        Me.Label37.Text = "Voltage"
        '
        'tbOverloadTime1
        '
        Me.tbOverloadTime1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.tbOverloadTime1.Location = New System.Drawing.Point(422, 4)
        Me.tbOverloadTime1.Margin = New System.Windows.Forms.Padding(0)
        Me.tbOverloadTime1.Name = "tbOverloadTime1"
        Me.tbOverloadTime1.Size = New System.Drawing.Size(65, 23)
        Me.tbOverloadTime1.TabIndex = 33
        '
        'tbVoltage1
        '
        Me.tbVoltage1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.tbVoltage1.Location = New System.Drawing.Point(166, 4)
        Me.tbVoltage1.Margin = New System.Windows.Forms.Padding(0)
        Me.tbVoltage1.Name = "tbVoltage1"
        Me.tbVoltage1.Size = New System.Drawing.Size(65, 23)
        Me.tbVoltage1.TabIndex = 30
        '
        'Label39
        '
        Me.Label39.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label39.AutoSize = true
        Me.Label39.Location = New System.Drawing.Point(51, 39)
        Me.Label39.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(108, 15)
        Me.Label39.TabIndex = 3
        Me.Label39.Text = "Continuous Torque"
        '
        'Label3
        '
        Me.Label3.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label3.AutoSize = true
        Me.Label3.Location = New System.Drawing.Point(16, 71)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(143, 15)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "Continuous Torque Speed"
        '
        'tbContinuousTorque1
        '
        Me.tbContinuousTorque1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.tbContinuousTorque1.Location = New System.Drawing.Point(166, 35)
        Me.tbContinuousTorque1.Margin = New System.Windows.Forms.Padding(0)
        Me.tbContinuousTorque1.Name = "tbContinuousTorque1"
        Me.tbContinuousTorque1.Size = New System.Drawing.Size(65, 23)
        Me.tbContinuousTorque1.TabIndex = 31
        '
        'tbContinuousTorqueSpeed1
        '
        Me.tbContinuousTorqueSpeed1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.tbContinuousTorqueSpeed1.Location = New System.Drawing.Point(166, 67)
        Me.tbContinuousTorqueSpeed1.Margin = New System.Windows.Forms.Padding(0)
        Me.tbContinuousTorqueSpeed1.Name = "tbContinuousTorqueSpeed1"
        Me.tbContinuousTorqueSpeed1.Size = New System.Drawing.Size(65, 23)
        Me.tbContinuousTorqueSpeed1.TabIndex = 32
        '
        'Label15
        '
        Me.Label15.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label15.AutoSize = true
        Me.Label15.Location = New System.Drawing.Point(235, 71)
        Me.Label15.Margin = New System.Windows.Forms.Padding(0)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(37, 15)
        Me.Label15.TabIndex = 67
        Me.Label15.Text = "[rpm]"
        '
        'Label42
        '
        Me.Label42.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label42.AutoSize = true
        Me.Label42.Location = New System.Drawing.Point(286, 71)
        Me.Label42.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(129, 15)
        Me.Label42.TabIndex = 9
        Me.Label42.Text = "Overload Torque Speed"
        '
        'Label14
        '
        Me.Label14.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label14.AutoSize = true
        Me.Label14.Location = New System.Drawing.Point(491, 71)
        Me.Label14.Margin = New System.Windows.Forms.Padding(0)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(37, 15)
        Me.Label14.TabIndex = 66
        Me.Label14.Text = "[rpm]"
        '
        'tbOverloadTorqueSpeed1
        '
        Me.tbOverloadTorqueSpeed1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.tbOverloadTorqueSpeed1.Location = New System.Drawing.Point(423, 67)
        Me.tbOverloadTorqueSpeed1.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.tbOverloadTorqueSpeed1.Name = "tbOverloadTorqueSpeed1"
        Me.tbOverloadTorqueSpeed1.Size = New System.Drawing.Size(64, 23)
        Me.tbOverloadTorqueSpeed1.TabIndex = 35
        '
        'Label41
        '
        Me.Label41.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label41.AutoSize = true
        Me.Label41.Location = New System.Drawing.Point(321, 39)
        Me.Label41.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(94, 15)
        Me.Label41.TabIndex = 7
        Me.Label41.Text = "Overload Torque"
        '
        'tbOverloadTorque1
        '
        Me.tbOverloadTorque1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.tbOverloadTorque1.Location = New System.Drawing.Point(423, 35)
        Me.tbOverloadTorque1.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.tbOverloadTorque1.Name = "tbOverloadTorque1"
        Me.tbOverloadTorque1.Size = New System.Drawing.Size(64, 23)
        Me.tbOverloadTorque1.TabIndex = 34
        '
        'Label12
        '
        Me.Label12.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label12.AutoSize = true
        Me.Label12.Location = New System.Drawing.Point(492, 39)
        Me.Label12.Margin = New System.Windows.Forms.Padding(0)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(35, 15)
        Me.Label12.TabIndex = 64
        Me.Label12.Text = "[Nm]"
        '
        'tpVoltageLevel
        '
        Me.tpVoltageLevel.Controls.Add(Me.GroupBox1)
        Me.tpVoltageLevel.Controls.Add(Me.FlowLayoutPanel10)
        Me.tpVoltageLevel.Controls.Add(Me.TableLayoutPanel4)
        Me.tpVoltageLevel.Location = New System.Drawing.Point(4, 24)
        Me.tpVoltageLevel.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.tpVoltageLevel.Name = "tpVoltageLevel"
        Me.tpVoltageLevel.Padding = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.tpVoltageLevel.Size = New System.Drawing.Size(595, 363)
        Me.tpVoltageLevel.TabIndex = 1
        Me.tpVoltageLevel.Text = "Voltage Level High"
        Me.tpVoltageLevel.UseVisualStyleBackColor = true
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.GroupBox1.Controls.Add(Me.TableLayoutPanel2)
        Me.GroupBox1.Location = New System.Drawing.Point(7, 145)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.GroupBox1.Size = New System.Drawing.Size(579, 208)
        Me.GroupBox1.TabIndex = 122
        Me.GroupBox1.TabStop = false
        Me.GroupBox1.Text = "Power Map Per Gear"
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.TableLayoutPanel2.ColumnCount = 2
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 419!))
        Me.TableLayoutPanel2.Controls.Add(Me.lvPowerMap2, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.FlowLayoutPanel4, 0, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.Label5, 1, 1)
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(7, 16)
        Me.TableLayoutPanel2.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 2
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 78.61636!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 21.38365!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(569, 183)
        Me.TableLayoutPanel2.TabIndex = 123
        '
        'lvPowerMap2
        '
        Me.lvPowerMap2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom)  _
            Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.lvPowerMap2.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2})
        Me.TableLayoutPanel2.SetColumnSpan(Me.lvPowerMap2, 2)
        Me.lvPowerMap2.FullRowSelect = true
        Me.lvPowerMap2.GridLines = true
        Me.lvPowerMap2.Location = New System.Drawing.Point(4, 3)
        Me.lvPowerMap2.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.lvPowerMap2.MultiSelect = false
        Me.lvPowerMap2.Name = "lvPowerMap2"
        Me.lvPowerMap2.Size = New System.Drawing.Size(561, 137)
        Me.lvPowerMap2.TabIndex = 77
        Me.lvPowerMap2.TabStop = false
        Me.lvPowerMap2.UseCompatibleStateImageBehavior = false
        Me.lvPowerMap2.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Gear #"
        Me.ColumnHeader1.Width = 59
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Power Map File Name"
        Me.ColumnHeader2.Width = 390
        '
        'FlowLayoutPanel4
        '
        Me.FlowLayoutPanel4.Controls.Add(Me.btAddPowerMap2)
        Me.FlowLayoutPanel4.Controls.Add(Me.btRemovePowerMap2)
        Me.FlowLayoutPanel4.Location = New System.Drawing.Point(4, 146)
        Me.FlowLayoutPanel4.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.FlowLayoutPanel4.Name = "FlowLayoutPanel4"
        Me.FlowLayoutPanel4.Size = New System.Drawing.Size(71, 32)
        Me.FlowLayoutPanel4.TabIndex = 124
        '
        'btAddPowerMap2
        '
        Me.btAddPowerMap2.Image = Global.TUGraz.VECTO.My.Resources.Resources.plus_circle_icon
        Me.btAddPowerMap2.Location = New System.Drawing.Point(4, 3)
        Me.btAddPowerMap2.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.btAddPowerMap2.Name = "btAddPowerMap2"
        Me.btAddPowerMap2.Size = New System.Drawing.Size(28, 28)
        Me.btAddPowerMap2.TabIndex = 48
        Me.btAddPowerMap2.UseVisualStyleBackColor = true
        '
        'btRemovePowerMap2
        '
        Me.btRemovePowerMap2.Image = Global.TUGraz.VECTO.My.Resources.Resources.minus_circle_icon
        Me.btRemovePowerMap2.Location = New System.Drawing.Point(4, 37)
        Me.btRemovePowerMap2.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.btRemovePowerMap2.Name = "btRemovePowerMap2"
        Me.btRemovePowerMap2.Size = New System.Drawing.Size(28, 28)
        Me.btRemovePowerMap2.TabIndex = 49
        Me.btRemovePowerMap2.UseVisualStyleBackColor = true
        '
        'Label5
        '
        Me.Label5.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.Label5.AutoSize = true
        Me.Label5.Location = New System.Drawing.Point(444, 143)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(121, 15)
        Me.Label5.TabIndex = 6
        Me.Label5.Text = "(Double-Click to Edit)"
        '
        'FlowLayoutPanel10
        '
        Me.FlowLayoutPanel10.Controls.Add(Me.Label28)
        Me.FlowLayoutPanel10.Controls.Add(Me.tbFLCurve2)
        Me.FlowLayoutPanel10.Controls.Add(Me.btFLCurveFile2)
        Me.FlowLayoutPanel10.Controls.Add(Me.btFLCurve2)
        Me.FlowLayoutPanel10.Location = New System.Drawing.Point(7, 108)
        Me.FlowLayoutPanel10.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.FlowLayoutPanel10.Name = "FlowLayoutPanel10"
        Me.FlowLayoutPanel10.Size = New System.Drawing.Size(579, 30)
        Me.FlowLayoutPanel10.TabIndex = 121
        '
        'Label28
        '
        Me.Label28.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label28.AutoSize = true
        Me.Label28.Location = New System.Drawing.Point(7, 7)
        Me.Label28.Margin = New System.Windows.Forms.Padding(7, 0, 10, 0)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(138, 15)
        Me.Label28.TabIndex = 59
        Me.Label28.Text = "Full Load Curve (.viepcp)"
        '
        'tbFLCurve2
        '
        Me.tbFLCurve2.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.tbFLCurve2.Location = New System.Drawing.Point(159, 3)
        Me.tbFLCurve2.Margin = New System.Windows.Forms.Padding(4, 3, 7, 3)
        Me.tbFLCurve2.Name = "tbFLCurve2"
        Me.tbFLCurve2.Size = New System.Drawing.Size(322, 23)
        Me.tbFLCurve2.TabIndex = 46
        '
        'btFLCurveFile2
        '
        Me.btFLCurveFile2.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btFLCurveFile2.Image = CType(resources.GetObject("btFLCurveFile2.Image"),System.Drawing.Image)
        Me.btFLCurveFile2.Location = New System.Drawing.Point(488, 0)
        Me.btFLCurveFile2.Margin = New System.Windows.Forms.Padding(0)
        Me.btFLCurveFile2.Name = "btFLCurveFile2"
        Me.btFLCurveFile2.Size = New System.Drawing.Size(28, 28)
        Me.btFLCurveFile2.TabIndex = 47
        Me.btFLCurveFile2.UseVisualStyleBackColor = true
        '
        'btFLCurve2
        '
        Me.btFLCurve2.Image = Global.TUGraz.VECTO.My.Resources.Resources.application_export_icon_small
        Me.btFLCurve2.Location = New System.Drawing.Point(516, 1)
        Me.btFLCurve2.Margin = New System.Windows.Forms.Padding(0, 1, 0, 0)
        Me.btFLCurve2.Name = "btFLCurve2"
        Me.btFLCurve2.Size = New System.Drawing.Size(28, 28)
        Me.btFLCurve2.TabIndex = 86
        Me.btFLCurve2.TabStop = false
        Me.btFLCurve2.UseVisualStyleBackColor = true
        '
        'TableLayoutPanel4
        '
        Me.TableLayoutPanel4.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.TableLayoutPanel4.ColumnCount = 7
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 163!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 72!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 37!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 147!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 72!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 37!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 67!))
        Me.TableLayoutPanel4.Controls.Add(Me.Label17, 2, 1)
        Me.TableLayoutPanel4.Controls.Add(Me.Label18, 2, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.Label26, 5, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.Label19, 0, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.tbOverloadTime2, 4, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.Label24, 3, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.tbVoltage2, 1, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.Label20, 0, 1)
        Me.TableLayoutPanel4.Controls.Add(Me.Label21, 0, 2)
        Me.TableLayoutPanel4.Controls.Add(Me.tbContinuousTorque2, 1, 1)
        Me.TableLayoutPanel4.Controls.Add(Me.tbContinuousTorqueSpeed2, 1, 2)
        Me.TableLayoutPanel4.Controls.Add(Me.Label27, 2, 2)
        Me.TableLayoutPanel4.Controls.Add(Me.Label23, 3, 2)
        Me.TableLayoutPanel4.Controls.Add(Me.tbOverloadTorqueSpeed2, 4, 2)
        Me.TableLayoutPanel4.Controls.Add(Me.Label25, 5, 2)
        Me.TableLayoutPanel4.Controls.Add(Me.Label22, 3, 1)
        Me.TableLayoutPanel4.Controls.Add(Me.tbOverloadTorque2, 4, 1)
        Me.TableLayoutPanel4.Controls.Add(Me.Label16, 5, 1)
        Me.TableLayoutPanel4.Location = New System.Drawing.Point(7, 8)
        Me.TableLayoutPanel4.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.TableLayoutPanel4.Name = "TableLayoutPanel4"
        Me.TableLayoutPanel4.RowCount = 3
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31!))
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31!))
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31!))
        Me.TableLayoutPanel4.Size = New System.Drawing.Size(579, 95)
        Me.TableLayoutPanel4.TabIndex = 120
        '
        'Label17
        '
        Me.Label17.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label17.AutoSize = true
        Me.Label17.Location = New System.Drawing.Point(236, 39)
        Me.Label17.Margin = New System.Windows.Forms.Padding(0)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(35, 15)
        Me.Label17.TabIndex = 63
        Me.Label17.Text = "[Nm]"
        '
        'Label18
        '
        Me.Label18.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label18.AutoSize = true
        Me.Label18.Location = New System.Drawing.Point(242, 8)
        Me.Label18.Margin = New System.Windows.Forms.Padding(0)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(22, 15)
        Me.Label18.TabIndex = 62
        Me.Label18.Text = "[V]"
        '
        'Label26
        '
        Me.Label26.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label26.AutoSize = true
        Me.Label26.Location = New System.Drawing.Point(499, 8)
        Me.Label26.Margin = New System.Windows.Forms.Padding(0)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(20, 15)
        Me.Label26.TabIndex = 65
        Me.Label26.Text = "[s]"
        '
        'Label19
        '
        Me.Label19.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label19.AutoSize = true
        Me.Label19.Location = New System.Drawing.Point(113, 8)
        Me.Label19.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(46, 15)
        Me.Label19.TabIndex = 1
        Me.Label19.Text = "Voltage"
        '
        'tbOverloadTime2
        '
        Me.tbOverloadTime2.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.tbOverloadTime2.Location = New System.Drawing.Point(423, 4)
        Me.tbOverloadTime2.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.tbOverloadTime2.Name = "tbOverloadTime2"
        Me.tbOverloadTime2.Size = New System.Drawing.Size(64, 23)
        Me.tbOverloadTime2.TabIndex = 43
        '
        'Label24
        '
        Me.Label24.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label24.AutoSize = true
        Me.Label24.Location = New System.Drawing.Point(331, 8)
        Me.Label24.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(84, 15)
        Me.Label24.TabIndex = 11
        Me.Label24.Text = "Overload Time"
        '
        'tbVoltage2
        '
        Me.tbVoltage2.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.tbVoltage2.Location = New System.Drawing.Point(167, 4)
        Me.tbVoltage2.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.tbVoltage2.Name = "tbVoltage2"
        Me.tbVoltage2.Size = New System.Drawing.Size(64, 23)
        Me.tbVoltage2.TabIndex = 40
        '
        'Label20
        '
        Me.Label20.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label20.AutoSize = true
        Me.Label20.Location = New System.Drawing.Point(51, 39)
        Me.Label20.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(108, 15)
        Me.Label20.TabIndex = 3
        Me.Label20.Text = "Continuous Torque"
        '
        'Label21
        '
        Me.Label21.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label21.AutoSize = true
        Me.Label21.Location = New System.Drawing.Point(16, 71)
        Me.Label21.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(143, 15)
        Me.Label21.TabIndex = 6
        Me.Label21.Text = "Continuous Torque Speed"
        '
        'tbContinuousTorque2
        '
        Me.tbContinuousTorque2.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.tbContinuousTorque2.Location = New System.Drawing.Point(167, 35)
        Me.tbContinuousTorque2.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.tbContinuousTorque2.Name = "tbContinuousTorque2"
        Me.tbContinuousTorque2.Size = New System.Drawing.Size(64, 23)
        Me.tbContinuousTorque2.TabIndex = 41
        '
        'tbContinuousTorqueSpeed2
        '
        Me.tbContinuousTorqueSpeed2.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.tbContinuousTorqueSpeed2.Location = New System.Drawing.Point(167, 67)
        Me.tbContinuousTorqueSpeed2.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.tbContinuousTorqueSpeed2.Name = "tbContinuousTorqueSpeed2"
        Me.tbContinuousTorqueSpeed2.Size = New System.Drawing.Size(64, 23)
        Me.tbContinuousTorqueSpeed2.TabIndex = 42
        '
        'Label27
        '
        Me.Label27.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label27.AutoSize = true
        Me.Label27.Location = New System.Drawing.Point(235, 71)
        Me.Label27.Margin = New System.Windows.Forms.Padding(0)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(37, 15)
        Me.Label27.TabIndex = 67
        Me.Label27.Text = "[rpm]"
        '
        'Label23
        '
        Me.Label23.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label23.AutoSize = true
        Me.Label23.Location = New System.Drawing.Point(286, 71)
        Me.Label23.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(129, 15)
        Me.Label23.TabIndex = 9
        Me.Label23.Text = "Overload Torque Speed"
        '
        'tbOverloadTorqueSpeed2
        '
        Me.tbOverloadTorqueSpeed2.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.tbOverloadTorqueSpeed2.Location = New System.Drawing.Point(423, 67)
        Me.tbOverloadTorqueSpeed2.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.tbOverloadTorqueSpeed2.Name = "tbOverloadTorqueSpeed2"
        Me.tbOverloadTorqueSpeed2.Size = New System.Drawing.Size(64, 23)
        Me.tbOverloadTorqueSpeed2.TabIndex = 45
        '
        'Label25
        '
        Me.Label25.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label25.AutoSize = true
        Me.Label25.Location = New System.Drawing.Point(491, 71)
        Me.Label25.Margin = New System.Windows.Forms.Padding(0)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(37, 15)
        Me.Label25.TabIndex = 66
        Me.Label25.Text = "[rpm]"
        '
        'Label22
        '
        Me.Label22.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label22.AutoSize = true
        Me.Label22.Location = New System.Drawing.Point(321, 39)
        Me.Label22.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(94, 15)
        Me.Label22.TabIndex = 7
        Me.Label22.Text = "Overload Torque"
        '
        'tbOverloadTorque2
        '
        Me.tbOverloadTorque2.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.tbOverloadTorque2.Location = New System.Drawing.Point(423, 35)
        Me.tbOverloadTorque2.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.tbOverloadTorque2.Name = "tbOverloadTorque2"
        Me.tbOverloadTorque2.Size = New System.Drawing.Size(64, 23)
        Me.tbOverloadTorque2.TabIndex = 44
        '
        'Label16
        '
        Me.Label16.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label16.AutoSize = true
        Me.Label16.Location = New System.Drawing.Point(492, 39)
        Me.Label16.Margin = New System.Windows.Forms.Padding(0)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(35, 15)
        Me.Label16.TabIndex = 64
        Me.Label16.Text = "[Nm]"
        '
        'ToolStrip1
        '
        Me.ToolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.ToolStrip1.ImageScalingSize = New System.Drawing.Size(24, 24)
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripBtNew, Me.ToolStripBtOpen, Me.ToolStripBtSave, Me.ToolStripBtSaveAs, Me.ToolStripSeparator3, Me.ToolStripBtSendTo, Me.ToolStripSeparator1, Me.ToolStripButton1})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(631, 31)
        Me.ToolStrip1.TabIndex = 78
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'ToolStripBtNew
        '
        Me.ToolStripBtNew.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripBtNew.Image = Global.TUGraz.VECTO.My.Resources.Resources.blue_document_icon
        Me.ToolStripBtNew.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripBtNew.Name = "ToolStripBtNew"
        Me.ToolStripBtNew.Size = New System.Drawing.Size(28, 28)
        Me.ToolStripBtNew.Text = "ToolStripButton1"
        Me.ToolStripBtNew.ToolTipText = "New"
        '
        'ToolStripBtOpen
        '
        Me.ToolStripBtOpen.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripBtOpen.Image = Global.TUGraz.VECTO.My.Resources.Resources.Open_icon
        Me.ToolStripBtOpen.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripBtOpen.Name = "ToolStripBtOpen"
        Me.ToolStripBtOpen.Size = New System.Drawing.Size(28, 28)
        Me.ToolStripBtOpen.Text = "ToolStripButton1"
        Me.ToolStripBtOpen.ToolTipText = "Open..."
        '
        'ToolStripBtSave
        '
        Me.ToolStripBtSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripBtSave.Image = Global.TUGraz.VECTO.My.Resources.Resources.Actions_document_save_icon
        Me.ToolStripBtSave.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripBtSave.Name = "ToolStripBtSave"
        Me.ToolStripBtSave.Size = New System.Drawing.Size(28, 28)
        Me.ToolStripBtSave.Text = "ToolStripButton1"
        Me.ToolStripBtSave.ToolTipText = "Save"
        '
        'ToolStripBtSaveAs
        '
        Me.ToolStripBtSaveAs.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripBtSaveAs.Image = Global.TUGraz.VECTO.My.Resources.Resources.Actions_document_save_as_icon
        Me.ToolStripBtSaveAs.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripBtSaveAs.Name = "ToolStripBtSaveAs"
        Me.ToolStripBtSaveAs.Size = New System.Drawing.Size(28, 28)
        Me.ToolStripBtSaveAs.Text = "ToolStripButton1"
        Me.ToolStripBtSaveAs.ToolTipText = "Save As..."
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(6, 31)
        '
        'ToolStripBtSendTo
        '
        Me.ToolStripBtSendTo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripBtSendTo.Image = Global.TUGraz.VECTO.My.Resources.Resources.export_icon
        Me.ToolStripBtSendTo.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripBtSendTo.Name = "ToolStripBtSendTo"
        Me.ToolStripBtSendTo.Size = New System.Drawing.Size(28, 28)
        Me.ToolStripBtSendTo.Text = "Send to Vehicle Editor"
        Me.ToolStripBtSendTo.ToolTipText = "Send to Vehicle Editor"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 31)
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton1.Image = Global.TUGraz.VECTO.My.Resources.Resources.Help_icon
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.Size = New System.Drawing.Size(28, 28)
        Me.ToolStripButton1.Text = "Help"
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.White
        Me.PictureBox1.Dock = System.Windows.Forms.DockStyle.Top
        Me.PictureBox1.Image = Global.TUGraz.VECTO.My.Resources.Resources.VECTO_Mainform
        Me.PictureBox1.Location = New System.Drawing.Point(0, 31)
        Me.PictureBox1.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(631, 46)
        Me.PictureBox1.TabIndex = 79
        Me.PictureBox1.TabStop = false
        '
        'lblTitle
        '
        Me.lblTitle.AutoSize = true
        Me.lblTitle.BackColor = System.Drawing.Color.White
        Me.lblTitle.Font = New System.Drawing.Font("Microsoft Sans Serif", 18!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.lblTitle.Location = New System.Drawing.Point(126, 43)
        Me.lblTitle.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(69, 29)
        Me.lblTitle.TabIndex = 80
        Me.lblTitle.Text = "IHPC"
        '
        'btCancel
        '
        Me.btCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.btCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btCancel.Location = New System.Drawing.Point(530, 662)
        Me.btCancel.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.btCancel.Name = "btCancel"
        Me.btCancel.Size = New System.Drawing.Size(88, 27)
        Me.btCancel.TabIndex = 51
        Me.btCancel.Text = "Cancel"
        Me.btCancel.UseVisualStyleBackColor = true
        '
        'btSave
        '
        Me.btSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.btSave.Location = New System.Drawing.Point(435, 662)
        Me.btSave.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.btSave.Name = "btSave"
        Me.btSave.Size = New System.Drawing.Size(88, 27)
        Me.btSave.TabIndex = 50
        Me.btSave.Text = "Save"
        Me.btSave.UseVisualStyleBackColor = true
        '
        'StatusStrip1
        '
        Me.StatusStrip1.ImageScalingSize = New System.Drawing.Size(24, 24)
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.LbStatus})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 692)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Padding = New System.Windows.Forms.Padding(1, 0, 16, 0)
        Me.StatusStrip1.Size = New System.Drawing.Size(631, 22)
        Me.StatusStrip1.SizingGrip = false
        Me.StatusStrip1.TabIndex = 83
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'LbStatus
        '
        Me.LbStatus.Name = "LbStatus"
        Me.LbStatus.Size = New System.Drawing.Size(39, 17)
        Me.LbStatus.Text = "Status"
        '
        'CmOpenFile
        '
        Me.CmOpenFile.ImageScalingSize = New System.Drawing.Size(24, 24)
        Me.CmOpenFile.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.OpenWithToolStripMenuItem, Me.ShowInFolderToolStripMenuItem})
        Me.CmOpenFile.Name = "CmOpenFile"
        Me.CmOpenFile.Size = New System.Drawing.Size(153, 48)
        '
        'OpenWithToolStripMenuItem
        '
        Me.OpenWithToolStripMenuItem.Name = "OpenWithToolStripMenuItem"
        Me.OpenWithToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.OpenWithToolStripMenuItem.Text = "Open with ..."
        '
        'ShowInFolderToolStripMenuItem
        '
        Me.ShowInFolderToolStripMenuItem.Name = "ShowInFolderToolStripMenuItem"
        Me.ShowInFolderToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.ShowInFolderToolStripMenuItem.Text = "Show in Folder"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.lblRatedPower)
        Me.Panel1.Controls.Add(Me.tbRatedPower)
        Me.Panel1.Controls.Add(Me.lblRatedPowerUnit)
        Me.Panel1.Location = New System.Drawing.Point(14, 154)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(248, 30)
        Me.Panel1.TabIndex = 102
        '
        'lblRatedPower
        '
        Me.lblRatedPower.AutoSize = true
        Me.lblRatedPower.Location = New System.Drawing.Point(4, 7)
        Me.lblRatedPower.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblRatedPower.Name = "lblRatedPower"
        Me.lblRatedPower.Size = New System.Drawing.Size(73, 15)
        Me.lblRatedPower.TabIndex = 45
        Me.lblRatedPower.Text = "Rated Power"
        '
        'tbRatedPower
        '
        Me.tbRatedPower.Location = New System.Drawing.Point(125, 3)
        Me.tbRatedPower.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.tbRatedPower.Name = "tbRatedPower"
        Me.tbRatedPower.Size = New System.Drawing.Size(66, 23)
        Me.tbRatedPower.TabIndex = 1
        '
        'lblRatedPowerUnit
        '
        Me.lblRatedPowerUnit.AutoSize = true
        Me.lblRatedPowerUnit.Location = New System.Drawing.Point(199, 7)
        Me.lblRatedPowerUnit.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblRatedPowerUnit.Name = "lblRatedPowerUnit"
        Me.lblRatedPowerUnit.Size = New System.Drawing.Size(32, 15)
        Me.lblRatedPowerUnit.TabIndex = 64
        Me.lblRatedPowerUnit.Text = "[kW]"
        '
        'IHPCForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7!, 15!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(631, 714)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.btCancel)
        Me.Controls.Add(Me.btSave)
        Me.Controls.Add(Me.lblTitle)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(Me.tcVoltageLevels)
        Me.Controls.Add(Me.pnThermalOverloadRecovery)
        Me.Controls.Add(Me.pnInertia)
        Me.Controls.Add(Me.FlowLayoutPanel2)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"),System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.MaximizeBox = false
        Me.Name = "IHPCForm"
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "IHPC"
        Me.FlowLayoutPanel1.ResumeLayout(false)
        Me.FlowLayoutPanel1.PerformLayout
        Me.pnInertia.ResumeLayout(false)
        Me.pnInertia.PerformLayout
        Me.pnThermalOverloadRecovery.ResumeLayout(false)
        Me.pnThermalOverloadRecovery.PerformLayout
        Me.FlowLayoutPanel2.ResumeLayout(false)
        Me.FlowLayoutPanel2.PerformLayout
        Me.tcVoltageLevels.ResumeLayout(false)
        Me.tpFirstVoltageLevel.ResumeLayout(false)
        Me.lvPowerMap.ResumeLayout(false)
        Me.TableLayoutPanel3.ResumeLayout(false)
        Me.TableLayoutPanel3.PerformLayout
        Me.FlowLayoutPanel11.ResumeLayout(false)
        Me.FlowLayoutPanel3.ResumeLayout(false)
        Me.FlowLayoutPanel3.PerformLayout
        Me.TableLayoutPanel1.ResumeLayout(false)
        Me.TableLayoutPanel1.PerformLayout
        Me.tpVoltageLevel.ResumeLayout(false)
        Me.GroupBox1.ResumeLayout(false)
        Me.TableLayoutPanel2.ResumeLayout(false)
        Me.TableLayoutPanel2.PerformLayout
        Me.FlowLayoutPanel4.ResumeLayout(false)
        Me.FlowLayoutPanel10.ResumeLayout(false)
        Me.FlowLayoutPanel10.PerformLayout
        Me.TableLayoutPanel4.ResumeLayout(false)
        Me.TableLayoutPanel4.PerformLayout
        Me.ToolStrip1.ResumeLayout(false)
        Me.ToolStrip1.PerformLayout
        CType(Me.PictureBox1,System.ComponentModel.ISupportInitialize).EndInit
        Me.StatusStrip1.ResumeLayout(false)
        Me.StatusStrip1.PerformLayout
        Me.CmOpenFile.ResumeLayout(false)
        Me.Panel1.ResumeLayout(false)
        Me.Panel1.PerformLayout
        Me.ResumeLayout(false)
        Me.PerformLayout

End Sub

    Friend WithEvents FlowLayoutPanel1 As FlowLayoutPanel
    Friend WithEvents Label49 As Label
    Friend WithEvents tbModel As TextBox
    Friend WithEvents pnInertia As Panel
    Friend WithEvents Label51 As Label
    Friend WithEvents tbInertia As TextBox
    Friend WithEvents lblinertiaUnit As Label
    Friend WithEvents pnThermalOverloadRecovery As FlowLayoutPanel
    Friend WithEvents Label52 As Label
    Friend WithEvents tbThermalOverload As TextBox
    Friend WithEvents Label10 As Label
    Friend WithEvents FlowLayoutPanel2 As FlowLayoutPanel
    Friend WithEvents Label1 As Label
    Friend WithEvents tbDragCurve As TextBox
    Friend WithEvents btDragCurve As Button
    Friend WithEvents tcVoltageLevels As TabControl
    Friend WithEvents tpFirstVoltageLevel As TabPage
    Friend WithEvents FlowLayoutPanel3 As FlowLayoutPanel
    Friend WithEvents Label2 As Label
    Friend WithEvents tbFLCurve1 As TextBox
    Friend WithEvents btFLCurveFile1 As Button
    Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
    Friend WithEvents Label11 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents Label43 As Label
    Friend WithEvents Label37 As Label
    Friend WithEvents tbOverloadTime1 As TextBox
    Friend WithEvents tbVoltage1 As TextBox
    Friend WithEvents Label39 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents tbContinuousTorque1 As TextBox
    Friend WithEvents tbContinuousTorqueSpeed1 As TextBox
    Friend WithEvents Label15 As Label
    Friend WithEvents Label42 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents tbOverloadTorqueSpeed1 As TextBox
    Friend WithEvents Label41 As Label
    Friend WithEvents tbOverloadTorque1 As TextBox
    Friend WithEvents Label12 As Label
    Friend WithEvents tpVoltageLevel As TabPage
    Friend WithEvents FlowLayoutPanel10 As FlowLayoutPanel
    Friend WithEvents Label28 As Label
    Friend WithEvents tbFLCurve2 As TextBox
    Friend WithEvents btFLCurveFile2 As Button
    Friend WithEvents TableLayoutPanel4 As TableLayoutPanel
    Friend WithEvents Label17 As Label
    Friend WithEvents Label18 As Label
    Friend WithEvents Label26 As Label
    Friend WithEvents Label19 As Label
    Friend WithEvents tbOverloadTime2 As TextBox
    Friend WithEvents Label24 As Label
    Friend WithEvents tbVoltage2 As TextBox
    Friend WithEvents Label20 As Label
    Friend WithEvents Label21 As Label
    Friend WithEvents tbContinuousTorque2 As TextBox
    Friend WithEvents tbContinuousTorqueSpeed2 As TextBox
    Friend WithEvents Label27 As Label
    Friend WithEvents Label23 As Label
    Friend WithEvents tbOverloadTorqueSpeed2 As TextBox
    Friend WithEvents Label25 As Label
    Friend WithEvents Label22 As Label
    Friend WithEvents tbOverloadTorque2 As TextBox
    Friend WithEvents Label16 As Label
    Friend WithEvents ToolStrip1 As ToolStrip
    Friend WithEvents ToolStripBtNew As ToolStripButton
    Friend WithEvents ToolStripBtOpen As ToolStripButton
    Friend WithEvents ToolStripBtSave As ToolStripButton
    Friend WithEvents ToolStripBtSaveAs As ToolStripButton
    Friend WithEvents ToolStripSeparator3 As ToolStripSeparator
    Friend WithEvents ToolStripBtSendTo As ToolStripButton
    Friend WithEvents ToolStripSeparator1 As ToolStripSeparator
    Friend WithEvents ToolStripButton1 As ToolStripButton
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents lblTitle As Label
    Friend WithEvents lvPowerMap As GroupBox
    Friend WithEvents TableLayoutPanel3 As TableLayoutPanel
    Friend WithEvents lvPowerMap1 As ListView
    Friend WithEvents ColumnHeader4 As ColumnHeader
    Friend WithEvents ColumnHeader5 As ColumnHeader
    Friend WithEvents FlowLayoutPanel11 As FlowLayoutPanel
    Friend WithEvents btAddPowerMap1 As Button
    Friend WithEvents btRemovePowerMap1 As Button
    Friend WithEvents Label7 As Label
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents TableLayoutPanel2 As TableLayoutPanel
    Friend WithEvents lvPowerMap2 As ListView
    Friend WithEvents ColumnHeader1 As ColumnHeader
    Friend WithEvents ColumnHeader2 As ColumnHeader
    Friend WithEvents FlowLayoutPanel4 As FlowLayoutPanel
    Friend WithEvents btAddPowerMap2 As Button
    Friend WithEvents btRemovePowerMap2 As Button
    Friend WithEvents Label5 As Label
    Friend WithEvents btCancel As Button
    Friend WithEvents btSave As Button
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents LbStatus As ToolStripStatusLabel
    Friend WithEvents btDragCurveOpen As Button
    Friend WithEvents btFLCurve1 As Button
    Friend WithEvents CmOpenFile As ContextMenuStrip
    Friend WithEvents OpenWithToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ShowInFolderToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents btFLCurve2 As Button
    Friend WithEvents Panel1 As Panel
    Friend WithEvents lblRatedPower As Label
    Friend WithEvents tbRatedPower As TextBox
    Friend WithEvents lblRatedPowerUnit As Label
End Class
