﻿using TUGraz.VectoCore.OutputData.XML.DeclarationReports.Common;

namespace TUGraz.VectoCore.OutputData.XML.DeclarationReports.VehicleInformationFile.VehicleInformationFile_0_1.ResultWriter
{
	public interface IVIFResultsWriterFactory : ICommonResultsWriterFactory { }
}