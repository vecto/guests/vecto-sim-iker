﻿using System.Windows.Controls;

namespace VECTO3GUI2020.Views.JobEditViews.Vehicle.Components
{
    /// <summary>
    /// Interaction logic for CommonComponentViewModel.xaml
    /// </summary>
    public partial class CommonComponentView : UserControl
    {
        public CommonComponentView()
        {
            InitializeComponent();
        }
    }
}
